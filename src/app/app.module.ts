import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserTransferStateModule} from "../modules/transfer-state/browser-transfer-state.module";
import {HomeComponent} from "./features/home/home.component";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
		BrowserModule.withServerTransition({appId: 'my-app'}),
		AppRoutingModule,
		BrowserTransferStateModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
