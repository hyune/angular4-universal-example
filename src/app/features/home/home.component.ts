import {Component, OnInit, PLATFORM_ID, Inject} from '@angular/core';
import {isPlatformBrowser} from "@angular/common";
import {TransferState} from "../../../modules/transfer-state/transfer-state";


@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

    public players: string[];

    constructor(@Inject(PLATFORM_ID) private platformId: Object, private transferState: TransferState) {}

    ngOnInit() {
        let isBrowser = isPlatformBrowser(this.platformId);

        if(isBrowser) {
            this.players = this.transferState.get("products");
            this.transferState.set("players", null);
        }

        if (!this.players) {

            this.players = new Array;
            this.players.push("Neymar");
            this.players.push("Cavani");
            this.players.push("Di Maria");

            if(!isBrowser) {
                this.transferState.set("players", this.players);
            }
        }
    }
}
